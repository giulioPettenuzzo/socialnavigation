#include "Robot.h"

using namespace std;

/*
 * default constructor: set a null value equal to 100000 to the position of the robot
 */
Robot::Robot(){
    x_coordinate = Robot::NULL_POSITION;
    y_coordinate = Robot::NULL_POSITION;
}
/*
 * constructor: set the position of the robot with the one given in param
 */
Robot::Robot(double x_coordinate, double y_coordinate){
    this -> x_coordinate = x_coordinate;
    this -> y_coordinate = y_coordinate;
}

/*
 * return the x coordinate of the robot current position
 */
double Robot::get_x_coordinate(){
  return x_coordinate;
}
/*
 * change the y coordinate of the robot current position with rhe one given in param
 */
void Robot::set_x_coordinate(double x_coordinate){
  this -> x_coordinate = x_coordinate;
}
/*
 * return the y coordinate of the robot current position
 */
double Robot::get_y_coordinate(){
  return y_coordinate;
}
/*
 * change the y coordinate of the robot current position with rhe one given in param
 */
void Robot::set_y_coordinate(double y_coordinate){
  this -> y_coordinate = y_coordinate;
}
