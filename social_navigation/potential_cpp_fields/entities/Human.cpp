#include "Human.h"

using namespace std;

/************* CONSTRUCTORS *************/
/*
 * The minimal constructor
 */
Human::Human(){
  id = 123456789;
  x_coordinate = Human::NULL_POSITION;
  y_coordinate = Human::NULL_POSITION;
}
/*
 * The minimal constructor
 */
Human::Human(int id){
  this -> id = id;
}

/************* GETTER AND SETTER *************/

/*
 * set the id of the human
 */
void Human::set_id(int id){
  this -> id = id;
}
/*
 * get the id of the human
 */
int Human::get_id(){
  return id;
}

/*
 * set the x coordinate of the human position
 */
void Human::set_x_coordinate(double x_coordinate)
{
    this -> x_coordinate = x_coordinate;
}
/*
 * get the x coordinate of the human position
 */
double Human::get_x_coordinate()
{
    return x_coordinate;
}

/*
 * set the y coordinate of the human position
 */
double Human::get_y_coordinate()
{
    return y_coordinate;
}
/*
 * get the y coordinate of the human position
 */
void Human::set_y_coordinate(double y_coordinate)
{
    this -> y_coordinate = y_coordinate;
}

/*
 * get the velocity of the human, express in m/s
 */
double Human::get_velocity()
{
    return velocity;
}
/*
 * set the velocity of the human, express in m/s
 */
void Human::set_velocity(double velocity)
{
    this -> velocity = velocity;
}

/*
 * get the direction which the human is moving, express in angle from 0 to 360
 */
double Human::get_direction()
{
    return direction;
}
/*
 * get the direction which the human is moving, express in angle from 0 to 360
 */
void Human::set_direction(double direction)
{
    this -> direction = direction;
}

/*
 * get the orientation of the body of the human , express in angle from 0 to 360
 */
double Human::get_pose_direction()
{
    return pose_direction;
}
/*
 * set the orientation of the body of the human, express in angle from 0 to 360
 */
void Human::set_pose_direction(double pose_direction)
{
    this -> pose_direction = pose_direction;
}
