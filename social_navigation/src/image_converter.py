#!/usr/bin/env python2.7
# Import ROS libraries and messages
import rospy
from sensor_msgs.msg import Image

# Import OpenCV libraries and tools
import cv2
import argparse
import logging
import sys
import time
import tf_pose

from tf_pose import common
import numpy as np
from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path, model_wh

from cv_bridge import CvBridge, CvBridgeError

# Print "Hello!" to terminal
print "Hello!"

# Initialize the ROS Node named 'opencv_example', allow multiple nodes to be run with this name
rospy.init_node('opencv_example', anonymous=True)

# Print "Hello ROS!" to the Terminal and to a ROS Log file located in ~/.ros/log/loghash/*.log

# Initialize the CvBridge class
bridge = CvBridge()
global image
frame = 0

# Define a function to show the image in an OpenCV Window
def show_image(img):
    cv2.namedWindow("Image Window", 1)
    cv2.imshow("Image Window", img)
    cv2.waitKey(3)

# Define a callback for the Image message
def image_callback(img_msg):
    # log some info about the image topic
    rospy.loginfo("Hello ROS!")

    rospy.loginfo(img_msg.header)

    # Try to convert the ROS Image message to a CV2 Image

    image = bridge.imgmsg_to_cv2(img_msg, "bgr8");
    image = image.resize(432,368);

    #THIS IS THE POSE ESTIMATION WITH OPENPOSE,
    #IF YOU REMOVE THE FOLLOWING 4 LINES EVERYTHING GOES FASTER
    e = TfPoseEstimator(get_graph_path("mobilenet_thin"), target_size=(432, 368))
    humans = e.inference(image)
    #image = TfPoseEstimator.draw_humans(image, humans, imgcopy=False, frame=)
    image = TfPoseEstimator.draw_humans(image, humans, imgcopy=False, frame=frame, output_json_dir="/home/giulio/Desktop/tmp")
    frame += 1

   # coco_style = tf_pose.infer(image)

    # Show the converted image
    show_image(image)

# Initalize a subscriber to the "/camera/rgb/image_raw" topic with the function "image_callback" as a callback
sub_image = rospy.Subscriber("/kinect/depth/image_raw", Image, image_callback)


# Loop to keep the program from shutting down unless ROS is shut down, or CTRL+C is pressed
while not rospy.is_shutdown():
    rospy.spin()
