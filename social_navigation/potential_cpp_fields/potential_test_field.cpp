#include "PotentialField.h"

int main(int argc, const char * argv[])
{
    Human human = Human(20);
    human.set_x_coordinate(30.3);
    human.set_y_coordinate(8.9);
    human.set_velocity(10.0);
    human.set_direction(10.0);
    human.set_pose_direction(60.3);

    std::cout << "HUMAN" << std::endl;
    std::cout << "id =  " << human.get_id() << std::endl;
    std::cout << "x_coordinate =  " << human.get_x_coordinate() << std::endl;
    std::cout << "y_coordinate =  " << human.get_y_coordinate() << std::endl;
    std::cout << "velocity =  " << human.get_velocity() << std::endl;
    std::cout << "direction =  " << human.get_direction() << std::endl;
    std::cout << "pose_direction =  " << human.get_pose_direction() << std::endl;

    Robot robot = Robot(30.3,30);
    std::cout << "ROBOT" << std::endl;
    std::cout << "x_coordinate =  " << robot.get_x_coordinate() << std::endl;
    std::cout << "y_coordinate =  " << robot.get_y_coordinate() << std::endl;

    PotentialField potentialField = PotentialField::Builder().
                                                set_KP_ATT(2).
                                                set_KAPPA(3).
                                                set_ALPHA(100).
                                                set_BETA(1).
                                                set_SIGMA(1).
                                                set_GAMMA(30).
                                                build();

    std::cout << "POTENTIAL FIELD" << std::endl;
    std::cout << "attractive_potential_field =  " << potentialField.get_attractive_force(robot,human) << std::endl << std::endl;
    potentialField.get_repulsive_force(robot,human);
    return 0;
}
