#include <iostream>
#include <math.h>
#include "entities/Human.h"
#include "entities/Robot.h"

/*
 *  This class represent an implementation of the attractive and repulsive potential field of Santoshi_Hoshino
 */
class PotentialField
{
  public:
    class Builder;

    double get_attractive_force(Robot robot, Human human);
    double get_repulsive_force(Robot robot, Human human);

    double get_vector_risk_x(double coefficient_of_risk, double theta, double distance, double mu);
    double get_vector_risk_y(double coefficient_of_risk, double theta, double distance, double mu);
  private:
    #define PI 3.14159265
    //These are coefficients to calculate the potential
    int KP_ATT;
    int KAPPA;
    int ALPHA;
    int BETA;
    int SIGMA;
    int GAMMA;

    PotentialField(int KP_ATT, int KAPPA, int ALPHA, int BETA, int SIGMA, int GAMMA);

    double get_bessel_0_order(int kappa);
    double get_coefficient_of_risk(Human human, Robot robot, double theta, double mu);
    double get_degree_of_velocity(Human human);
    double get_relative_distance(Robot robot, Human human);
    double get_angle(Robot robot, Human human);
};

/***************** BUILDER ***********************/
class PotentialField::Builder{
  public:
    Builder(){
      KP_ATT = 2; //the coefficient of attractive in the formula of Santoshi_Hoshino
      KAPPA = 3; //is a measure of concentration on the moving direction derived from SD × ||vh||
      ALPHA = 100; //is the coefficient
      BETA = 1; //is a coefficient of the velocity vh
      SIGMA = 1; //is the variance of the radial component
      GAMMA = 30;
    }
    // sets custom values for Product creation
    // returns Builder for shorthand inline usage (same way as cout <<)
    Builder& set_KP_ATT( int KP_ATT ){ this->KP_ATT = KP_ATT; return *this; }
    Builder& set_KAPPA( int KAPPA ){ this->KAPPA = KAPPA; return *this; }
    Builder& set_ALPHA( int ALPHA ){ this->ALPHA = ALPHA; return *this; }
    Builder& set_BETA( int BETA ){ this->BETA = BETA; return *this; }
    Builder& set_SIGMA( int SIGMA ){ this->SIGMA = SIGMA; return *this; }
    Builder& set_GAMMA( int GAMMA ){ this->GAMMA = GAMMA; return *this; }

    // produce desired Product
    PotentialField build()
    {
       return PotentialField( KP_ATT, KAPPA, ALPHA, BETA, SIGMA, GAMMA);
    }

  private:
    // variables needed for construction of object of Product class
    int KP_ATT; //the coefficient of attractive in the formula of Santoshi_Hoshino
    int KAPPA; //is a measure of concentration on the moving direction derived from SD × ||vh||
    int ALPHA; //is the coefficient
    int BETA; //is a coefficient of the velocity vh
    int SIGMA; //is the variance of the radial component
    int GAMMA;
};
