  #include "PotentialField.h"

  PotentialField::PotentialField(int KP_ATT,int KAPPA,int ALPHA,int BETA,int SIGMA,int GAMMA)
  {
    this->KP_ATT = KP_ATT;
    this->KAPPA = KAPPA;
    this->ALPHA = ALPHA;
    this->BETA = BETA;
    this->SIGMA = SIGMA;
    this->GAMMA = GAMMA;
  }
  /*
   * compute the attractive force in the current position of the robot given the following formula (Santoshi_Hoshino)
   * Uxd(x) = kp(x − xd)
   * where "kp" is a coefficient, "x" and "xd" represent the positions of a robot and its destination (in this case the human).
   * Hence the robot in "Uxd" is affected by the following constant attractive force regardless of the positions: Fxd = −∇Uxd
   */
  double PotentialField::get_attractive_force(Robot robot, Human human)
  {
    // compute euclidean distanche between humn and robot
    double relative_distance = get_relative_distance(robot,human);
    // compute the euclidean distance
    return KP_ATT*relative_distance;
  }

  /*
   * compute the repulsive force in the current position of the robot given the following formula (Santoshi_Hoshino)
   * return the value of the potential field in the position of the robot
   */
  double PotentialField::get_repulsive_force(Robot robot, Human human)
  {
    //parameters
    double distance = get_relative_distance(robot,human);
    double theta = get_angle(robot,human);

    double mu = get_degree_of_velocity(human);
    std::cout << "mu =  " << mu << std::endl;

    //coefficient and potential under the robot position
    double coefficient = get_coefficient_of_risk(human, robot, theta, mu);
    std::cout << "coefficient =  " << coefficient << std::endl;

    double f_x = get_vector_risk_x(coefficient, theta, distance, mu);
    std::cout << "x force =  " << f_x << std::endl;

    double f_y = get_vector_risk_y(coefficient, theta, distance, mu);
    std::cout << "y force =  " << f_y << std::endl;

    double repulsive_potential_field = sqrt(pow(f_x,2) + pow(f_y,2));
    std::cout << "repulsive_potential_field =  " << repulsive_potential_field << std::endl;
    return repulsive_potential_field;
  }


  /*
   *  polynomial approximation of the zeroth order modified Bessel function
   *  from the Numerical Recipes in C p. 237
   */
  double PotentialField::get_bessel_0_order(int kappa)
  {
    double ax,ans;
	  double y;

	  ax=fabs(kappa);
	  if (ax < 3.75)
	  {
		    y=kappa/3.75;
		    y*=y;
		    ans=1.0+y*(3.5156229+y*(3.0899424+y*(1.2067492+y*(0.2659732+y*(0.360768e-1+y*0.45813e-2)))));
	  }
	  else
	  {
		  y=3.75/ax;
		  ans=(exp(ax)/sqrt(ax))*(0.39894228+y*(0.1328592e-1+y*(0.225319e-2+y*(-0.157565e-2+y*(0.916281e-2+y*(-0.2057706e-1+y*(0.2635537e-1+y*(-0.1647633e-1+y*0.392377e-2))))))));
	  }
	  return ans;
  }

  /*
   * compute the coefficient of risk, as formula 7 cap "Behavior potential field" of "Santoshi_Hoshino"
   * without the jacobian matrix which will be calculate in get_vector_risk_x() and get_vector_risk_y methods
   */
  double PotentialField::get_coefficient_of_risk(Human human, Robot robot, double theta, double mu)
  {
    double bessel = get_bessel_0_order(KAPPA);
    //human velocity is assumed to be known and in module
    double human_velocity = human.get_velocity();
    //distance between human and robot
    double distance = get_relative_distance(robot,human);
    std::cout << "KP_ATT =  " << KP_ATT << std::endl;
    std::cout << "KAPPA =  " << KAPPA << std::endl;
    std::cout << "ALPHA =  " << ALPHA << std::endl;
    std::cout << "BETA =  " << BETA << std::endl;
    std::cout << "SIGMA =  " << SIGMA << std::endl;
    std::cout << "GAMMA =  " << GAMMA << std::endl;


    return ALPHA * BETA * human_velocity * exp(KAPPA*cos(theta - mu) - distance/(2*SIGMA))/(4*(PI*PI)*bessel*SIGMA);
  }

  /*
   * multiply the coefficient of risk given in param with the first raw of the jacobian matrix
   * the formula is implemented as the one in fig.8 cap "Behavior potential field" of "Santoshi_Hoshino"
   * @param theta = angle between human and ROBOT
   * @param distance = distance between human and robot
   * return the potential affects the repulsive force on j th robot in the x direction
   */
  double PotentialField::get_vector_risk_x(double coefficient_of_risk, double theta, double distance, double mu)
  {
    return (-1) * coefficient_of_risk * (-1 * cos(theta)/(2*SIGMA) + KAPPA * sin(theta)*sin(theta - mu)/distance);
  }

  /*
   * multiply the coefficient of risk given in param with the second raw of the jacobian matrix
   * the formula is implemented as the one in fig.8 cap "Behavior potential field" of "Santoshi_Hoshino"
   * @param theta = angle between human and ROBOT
   * @param distance = distance between human and robot
   * return the potential affects the repulsive force on j th robot in the y direction
   */
  double PotentialField::get_vector_risk_y(double coefficient_of_risk, double theta, double distance, double mu)
  {
    return (-1) * coefficient_of_risk * (-1 * sin(theta)/(2*SIGMA) - KAPPA*cos(theta)*sin(theta - mu)/distance);
  }

  /*
   * return the degree of velocity of the human (mu coefficient)
   *        -1 if the velocity of the human is not known
   */
   double PotentialField::get_degree_of_velocity(Human human)
   {
     //check the parameters
     if(human.get_x_coordinate() == Human::NULL_POSITION || human.get_y_coordinate() == Human::NULL_POSITION){
       return -1;
     }
     return atan2(human.get_velocity() * sin(human.get_direction()), human.get_velocity() * cos(human.get_direction()));
   }


  /*
   * return * the relative distance between the robot and the human given in param
   *        * (-1) if the position of the robot is unknown
   *        * (-2) if the position of the human is unknown
   */
  double PotentialField::get_relative_distance(Robot robot, Human human)
  {
    //check the parameters
    if(robot.get_x_coordinate() == Robot::NULL_POSITION || robot.get_y_coordinate() == Robot::NULL_POSITION){
      return -1;
    }
    if(human.get_x_coordinate() == Human::NULL_POSITION || human.get_y_coordinate() == Human::NULL_POSITION){
      return -2;
    }

    //euclidean distance
    return sqrt(pow(robot.get_x_coordinate() - human.get_x_coordinate(),2) + pow(robot.get_y_coordinate() - human.get_y_coordinate(),2));
  }

  /*
   * Return * the Principal arc tangent of y/x, in the interval [-pi,+pi] radians.
   *          One radian is equivalent to 180/PI degrees
   *        * (-1) if the position of the robot is unknown
   *        * (-2) if the position of the human is unknown
   */
  double PotentialField::get_angle(Robot robot, Human human)
  {
    //check the parameters
    if(robot.get_x_coordinate() == Robot::NULL_POSITION || robot.get_y_coordinate() == Robot::NULL_POSITION){
      return -1;
    }
    if(human.get_x_coordinate() == Human::NULL_POSITION || human.get_y_coordinate() == Human::NULL_POSITION){
      return -2;
    }

    //arctang between human and robot
    return atan2 (robot.get_y_coordinate() - human.get_y_coordinate(),robot.get_x_coordinate() - human.get_x_coordinate()) * 180 / PI;
  }
