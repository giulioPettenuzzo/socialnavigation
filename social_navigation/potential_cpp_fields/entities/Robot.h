#include <iostream>

class Robot
{
  public:
    static const int NULL_POSITION = -100000;

    Robot();
    Robot(double x_coordinate, double y_coordinate);

    double get_x_coordinate();
    void set_x_coordinate(double x_coordinate);
    double get_y_coordinate();
    void set_y_coordinate(double y_coordinate);
  private:
    double x_coordinate;
    double y_coordinate;
};
