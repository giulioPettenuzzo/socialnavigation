#include <iostream>

class Human
{
  public:
    static const int NULL_POSITION = -100000;

    Human();
    Human(int id);

    int get_id();
    void set_id(int id);
    double get_x_coordinate();
    void set_x_coordinate(double y_coordinate);
    double get_y_coordinate();
    void set_y_coordinate(double y_coordinate);
    double get_velocity();
    void set_velocity(double velocity);
    double get_direction();
    void set_direction(double direction);
    double get_pose_direction();
    void set_pose_direction(double pose_direction);

  private:
    int id; //id of the human, the robot will assign the id to a human after have seen him
    double x_coordinate; //x coodrinate of the human
    double y_coordinate; //y coordinate of the Human
    double velocity; //velocity of the human
    double direction; //direction of the human
    double pose_direction; //the orientation of the body of the human
};
